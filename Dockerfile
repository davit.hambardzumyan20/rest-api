FROM python:3.9
WORKDIR /code
COPY requirements.txt /code/requirements.txt
RUN pip install pymongo[srv]
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt


