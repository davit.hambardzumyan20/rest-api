up:
	docker-compose up -d
run:
	C:\Users\User\AppData\Local\Programs\Python\Python39\python.exe -m uvicorn src.main:app --host 0.0.0.0 --port 80
stop:
	docker-compose stop
down:
	docker-compose down
build:
	docker build -t myimage .
diff:
	docker exec -it php-app-container php ./bin/console doctrine:migrations:diff --no-interaction
migrate:
	docker exec -it php-app-container php ./bin/console doctrine:migrations:migrate --no-interaction
cache-clear:
	docker exec -it php-app-container php bin/console cache:clear
cache-warmup:
	docker exec -it php-app-container php bin/console cache:warmup
consume:
	docker exec -it php-app-container php bin/console messenger:consume async async_feedback_sqs async_license_review_sqs -vv
doc:
	docker run -p 8083:8080  -d -e SWAGGER_JSON=/api.yaml -v `pwd`/public/api.yaml:/api.yaml swaggerapi/swagger-ui
stan:
	docker exec -it php-app-container ./vendor/bin/phpstan analyse -l 8 src --memory-limit=1G
phpcs:
	docker exec -it php-app-container ./vendor/bin/phpcs -n src -s
fix:
	docker exec -it php-app-container ./vendor/bin/phpcbf src
phpmd:
	docker exec -it php-app-container ./vendor/bin/phpmd ./src json cleancode, codesize, controversial, design, naming, unusedcode
test:
	docker exec -it php-app-container php vendor/bin/codecept run
coverage:
	docker exec -it php-app-container php -dxdebug.mode=coverage vendor/bin/codecept run --coverage
make:
	docker exec -it php-app-container php bin/console app:command:delete-unattached-videos