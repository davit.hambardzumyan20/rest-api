import yaml
import os
from dotenv import load_dotenv
load_dotenv()


class ParameterBag:

    __instance = None

    def __init__(self):
        with open('./app/Config/config.yaml', 'rb') as f:
            self.conf = yaml.load(f.read(), Loader=yaml.FullLoader)
        ParameterBag.__instance = self

    @staticmethod
    def getInstance():
        """ Static access method. """
        if ParameterBag.__instance is None:
            ParameterBag()
        return ParameterBag.__instance

    def get(self, name: str):
        if name in self.conf:
            return self.conf[name]

        return None

