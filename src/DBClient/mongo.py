from pymongo import MongoClient
import os


class MongoRepository:

    mongo: MongoClient

    def __init__(self):
        self.mongo = MongoClient(os.getenv("MONGO_TOKEN"))
        self.table = self.mongo[os.getenv("MONGO_TABLE")]
        self.collection = self.table[os.getenv("MONGO_COLLECTION")]
        self.hash_collection = self.table[os.getenv("MONGO_HASH_COLLECTION")]



    def save(self, data: object, _id: str, hash: str):
        self.collection.save({"_id": _id, "data": data, "hash": hash})

    def get_by_id(self, _id: str):
        return self.collection.find(_id)

    def get_hash_count(self, item: str):
        print(item)
        return self.collection.count({"hash": item})

    def get_hash_data(self, data_hash: str):
        return self.hash_collection.find({"_id": data_hash})

    def make_new_hash_data(self, data_hash, db_type, table_name):
        self.hash_collection.save({'_id': data_hash, "db_type": db_type, "table_name": table_name})

    def get_one_by_hash(self, data_hash):
        self.collection.find_one({"hash": data_hash})

    def get_all_by_hash(self, data_hash):
        self.collection.find({"hash": data_hash})

    def delete_by_id(self, _id):
        self.collection.delete_one({"_id": _id})
