import os
import mysql.connector

from app.DataManager.encoder import Encoder

GCP_PROJECT_ID = os.getenv('GCP_PROJECT_ID')
SERVICE_ACCOUNT_FILE = os.getenv('SERVICE_ACCOUNT_FILE')
STORAGE_BUCKET_NAME = os.getenv('STORAGE_BUCKET_NAME')
print(os.getenv('GCP_PROJECT_ID'))


class QueryBuilder:

    query = ''
    field_type_to_query_map = {
        "integer": "int",
        "string": "TEXT(255)",
        "bool": "BOOLEAN",
        "obj": "json"
    }

    def __init__(self, name):
        self.query += f"CREATE TABLE {name} (id CHAR(36) PRIMARY KEY AUTO_INCREMENT"

    def getQuery(self):
        return self.query + ")"

    def addField(self, field_name, field_type):
        self.query += "," + field_name + self.getType(field_type)

    def getType(self, field_type):
        return self.field_type_to_query_map[field_type]

class InsertQueryBuilder:

    query = ''
    values = ''
    field_type_to_query_map = {
        "<class 'int'>": "INT",
        "<class 'bool'>": "BOOLEAN",
        "<class 'str'>": "TEXT(255)",
        "<class 'dict'>": "json"
    }

    def __init__(self, name):
        self.query += f"INSERT INTO {name} ( id"

    def getQuery(self, data):
        values = f"VALUES ( {data['_id']}"
        keys = data.keys()
        for key in keys:
            self.query += f", {key}"
            values += data[key]

        return self.query + ")" + values + ")"


class SqlRepository:

    encoder = Encoder()
    cnx = mysql.connector.connect(user=os.getenv("MYSQL_USER"), password=os.getenv("MYSQL_PASSWORD"),
                                  host=os.getenv("MYSQL_HOST"),
                                  database=os.getenv("MYSQL_DATABASE"))

    def save(self, data, data_hash):

        qb = InsertQueryBuilder(data_hash)

        self.cnx.execute(qb.getQuery(data))

    def create_table(self, data, data_hash):
        data_string = self.encoder.get_string(data)
        data_fields = data_string.split(",")

        query = QueryBuilder("data_hash")
        for field in data_fields:
            field_data = field.split(":")
            query.addField(field_data[0], field_data[1])
        # self.cnx.execute(query.getQuery())

        return data_hash


