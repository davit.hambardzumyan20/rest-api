import queue
import threading
import time
from app.DBClient.mongo import MongoRepository
from app.DBClient.sql import SqlRepository
from app.Config.parameter_bag import ParameterBag
from app.DataManager.encoder import Encoder
import json

class HandlerService:
    sql = SqlRepository()
    mongo = MongoRepository()
    config = ParameterBag.getInstance()
    encoder = Encoder()


    def __init__(self, hash_queue: queue.Queue):
        self.hash_queue = hash_queue

    def data_place_change(self):
        pass




    def worker(self):
        while True:
            while not self.hash_queue.empty():

                data = json.loads(self.hash_queue.get())

                print(data)
                data_hash = self.encoder.encode(data["data"])
                print(data_hash)
                self.mongo.save(data["data"], data["_id"], data_hash)
                count_hash = self.mongo.get_hash_count(data_hash)

                item = data_hash
                if count_hash > self.config.get('TOTAL_DATA_COUNT'):
                    simple_data = self.mongo.get_one_by_hash(item)
                    name = self.sql.create_table(item, simple_data)
                    self.mongo.make_new_hash_data(item, 'sql', name)
                    self.data_place_change()
                    all_data = self.mongo.get_all_by_hash()

                    for simple_data in all_data:
                        self.sql.save(simple_data, name)
                        self.mongo.delte_by_id(simple_data["_id"])
            print("queue empty")
            time.sleep(2)


hash_queue = queue.Queue()

def start_handle():
    global hash_queue
    service = HandlerService(hash_queue)

    threading.Thread(target=service.worker, daemon=True).start()

    return hash_queue

