import hashlib


class Encoder:

    def encode(self, data):
        data_string = self.get_string(data)
        print(data_string)
        print(data_string)
        print(data_string)
        print(data_string)

        return hashlib.sha224(data_string.encode('utf-8')).hexdigest()

    def get_string(self, data):
        data_key_type = []
        for key in data:
            data_key_type.append(key + ':' + str(type(data[key])))
        data_key_type.sort()
        data_string = ''
        for data_type in data_key_type:
            data_string += data_type + ","

        return data_string
