from app.DataManager.encoder import Encoder
from app.DBClient.mongo import MongoRepository
from app.DataManager.data_hash_queue import hash_queue
from app.DBClient.sql import SqlRepository

class Manager:

    encoder = Encoder()
    mongo = MongoRepository()
    sql = SqlRepository()


    def save(self, data_from_kafka):
        data_body = data_from_kafka["data"]
        _id = data_from_kafka["_id"]

        data_hash = self.encoder.encode(data_body)
        hash_save_data = self.mongo.get_hash_data(data_hash)
        if hash_save_data is None:
            self.mongo.save(data_body, _id, data_hash)
            hash_queue.put(data_hash)
        else:
            if hash_save_data["db_type"] == "sql":
                self.save_sql(data_body, _id, hash_save_data["table_name"])

    def save_sql(self, data, _id, tb_name):
        self.sql.save(data, _id, tb_name)
