from kafka import KafkaConsumer
from json import loads
from time import sleep
from app.DataManager.manager import Manager
import os


class Consumer:
    pass

    consumer = KafkaConsumer(
        'topic_test',
        bootstrap_servers=[os.getenv("KAFKA_SERVER")],
        auto_offset_reset=os.getenv("OFFSET_RESET"),
        enable_auto_commit=True,
        group_id=os.getenv("GROUP_ID"),
        value_deserializer=lambda x: loads(x.decode('utf-8'))
    )

    manager = Manager()

    def consume(self):
        while True:
            for event in self.consumer:
                event_data = event.value
                self.manager.save(event_data)
