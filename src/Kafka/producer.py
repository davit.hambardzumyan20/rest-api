from json import dumps
from kafka import KafkaProducer
import os
import uuid


class Producer:
    pass
    producer = KafkaProducer(
        bootstrap_servers=[os.getenv("KAFKA_SERVER")],
        value_serializer=lambda x: dumps(x).encode('utf-8'),
    )

    def produce(self, value: object, _id: uuid):
        a = self.producer.send('topic_test', value={"data": value, "_id": str(_id)})


