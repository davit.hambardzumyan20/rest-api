from app.Config.parameter_bag import ParameterBag


class Authentication:

    params: ParameterBag

    def __init__(self):
        self.params = ParameterBag.getInstance()

    def validate(self, token):
        print(self.params.get('access_token'))
        print(type(self.params.get('access_token')))
        return token in self.params.get('access_token')


