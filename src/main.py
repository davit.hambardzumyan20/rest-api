from pydantic import BaseModel
from fastapi import FastAPI, Depends
from fastapi.security import HTTPBearer
from app.Security.Secure import Authentication
from app.Kafka.producer import Producer
from app.DBClient.mongo import MongoRepository
from app.setup import start
from app.DataManager.data_hash_queue import hash_queue
import json
import uuid
import os

app = FastAPI()
start()
bearer = HTTPBearer()
authentication = Authentication()
# data_producer = Producer()


class Data(BaseModel):
    data: object


@app.post("/storages")
def post_data(data: Data, token=Depends(bearer)):

    if not authentication.validate(token.credentials):
        return {"error": token}
    print('5555555555555555555');
    _id = uuid.uuid1()
    hash_queue.put(json.dumps({"data": data.data, "_id": str(_id)}))
    # data_producer.produce(data.data, _id)

    return {"key": _id}


mongo_client = MongoRepository()

@app.get("/data-storages")
def get_data(key: str, token=Depends(bearer)):
    if authentication.validate(token.credentials):
        return {"token": token}

    return mongo_client.get_by_id(key)

